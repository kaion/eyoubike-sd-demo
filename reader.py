#!/usr/bin/python
# -*- coding: utf-8 -*-
import serial

rs232 = {'port':'/dev/ttyS0','baudrate':'115200','bytesize':8,'parity':'N','stopbits':1,'timeout':0.01}
console = serial.Serial(port=rs232['port'], baudrate=rs232['baudrate'], bytesize=rs232['bytesize'], parity=rs232['parity'], 
                        stopbits=rs232['stopbits'], timeout=rs232['timeout'], xonxoff=0, rtscts=0)

LEFT_CARD =  1
LEFT_TAG =   2
RIGHT_CARD = 3
RIGHT_TAG =  4

target = [("LEFT_CARD",'\x27\x05\x01\x00'),("LEFT_TAG",'\x27\x05\x02\x00'),
          ("RIGHT_CARD",'\x27\x05\x03\x00'),("RIGHT_TAG",'\x27\x05\x04\x00')]


def chksum(data):
  chksum = 0x00
  for i in range( 0, len(data)):
      chksum = chksum ^ ord(data[i])
  return chr(chksum)

def request( data ):
  data = '\x00\x00' + chr(len(data)) + data
  data += chksum( data )
  console.write( data )
  #print 'Send(%3d): ' % len(data) + ' '.join('{:02X}'.format(ord(c)) for c in (data))
  response_data = ''
  timeout = 0
  length = 0
  while True:
    rb = console.read(1)
    if len(rb) != 0:
      timeout = 0
      response_data += rb
      now_l = len(response_data)
      if now_l == 3:
        length = ord(response_data[2])
      elif now_l >= 4 and now_l == length + 4: # 4 => INS CLS LEN CHK
        break
    else:
      timeout += 1
    if timeout >= 100:
      break
  #print 'Recv(%3d): ' % len(response_data) + ' '.join('{:02X}'.format(ord(c)) for c in response_data)
  return '-'.join('{:02X}'.format(ord(c)) for c in response_data[3:length+3]) #response_data[3:length+3]

def action( direction="left", purpose="tag" ):
  #if direction is LEFT_CARD:
  #  return request('\x27\x05\x01\x00')
  #elif direction is LEFT_TAG:
  #  return request('\x27\x05\x02\x00')
  #elif direction is RIGHT_CARD:
  #  return request('\x27\x05\x03\x00')
  #elif direction is RIGHT_TAG:
  #  return request('\x27\x05\x04\x00')
  #return False
  for x in target:
    if x[0] == ( direction + '_' + purpose ).upper():
      return request(x[1])
  return False
    
if __name__ == '__main__':
  print ' '.join('{:02X}'.format(ord(c)) for c in action("LEFT_CARD"))
  print ' '.join('{:02X}'.format(ord(c)) for c in action("LEFT_TAG"))
  print ' '.join('{:02X}'.format(ord(c)) for c in action("RIGHT_CARD"))
  print ' '.join('{:02X}'.format(ord(c)) for c in action("RIGHT_TAG"))