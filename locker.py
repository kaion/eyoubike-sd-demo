#!/usr/bin/python
# -*- coding: utf-8 -*-
from  gpio_mux import *
import time

Lock = 0
Unlock = 1

wait_t = 2 #馬達最多轉幾秒

def action( direction, purpose ):
  direction = direction.upper()
  purpose = purpose.upper()
  #print purpose
  if lockerCheck( direction ):
    checkpoint = Right_Lock if direction == 'RIGHT' else Left_Lock
    if purpose == 'UNLOCK':
      #print '-test-'
      motor = Right_Motor_P if direction == 'RIGHT' else Left_Motor_P
      check_status = True
    else: #Lock
      motor = Right_Motor_N if direction == 'RIGHT' else Left_Motor_N
      check_status = False
    #print "checkpoint: %d, motor: %d" % (checkpoint,motor)
    return motorACT( checkpoint, motor, check_status )
  return False

def lockerCheck( direction ):
  existpoint = Right_Lock_Open if direction == 'RIGHT' else Left_Lock_Open
  locker_existed = False if gpio.input(existpoint) else True
  if locker_existed is False:
    return False # locker is not existed
  return True

def motorACT( checkpoint, motor, check_status ):
  thislock = False if gpio.input(checkpoint) else True
  return_code = True
  #print "thislock: %s" % thislock
  if thislock is check_status: 
    gpio.output(motor, 1)
    timeout = ( time.time() + wait_t ) * 1000
    #print "timeout: %f" % timeout
    while thislock is check_status:
      time.sleep(0.1)
      now_t = time.time() * 1000
      #print "now_t:   %f" % now_t
      if now_t > timeout:
        return_code = False
        break
      thislock = False if gpio.input(checkpoint) else True
    gpio.output(motor, 0)
  return return_code
  
if __name__ == '__main__':
  dir = 'Left'
  print action( dir, 'Lock' )
  time.sleep(3)
  print action( dir, 'Unlock' )
