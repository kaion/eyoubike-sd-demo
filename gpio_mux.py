#!/usr/bin/python
# -*- coding: utf-8 -*-
import gpio

BUZZER_CTL_EN = 35

Right_Motor_P	= 45
Right_Motor_N	= 46
Left_Motor_P	= 47
Left_Motor_N	= 48

Left_Lock_Open	= 44
Left_Lock	= 49
Left_Bike	= 50

Right_Lock_Open	= 51
Right_Lock	= 52
Right_Bike	= 53

Left_LED_IN2 = 3
Left_LED_IN1 = 4
Right_LED_IN2 = 6
Right_LED_IN1 = 7

LCM_BL = 2
LCM_DP = 0

gpio.setup(BUZZER_CTL_EN, gpio.OUT)
gpio.setup(Right_Motor_P, gpio.OUT)
gpio.setup(Right_Motor_N, gpio.OUT)
gpio.setup(Left_Motor_P, gpio.OUT)
gpio.setup(Left_Motor_N, gpio.OUT)
gpio.setup(Left_Lock_Open, gpio.IN)
gpio.setup(Left_Lock, gpio.IN)
gpio.setup(Left_Bike, gpio.IN)
gpio.setup(Right_Lock_Open, gpio.IN)
gpio.setup(Right_Lock, gpio.IN)
gpio.setup(Right_Bike, gpio.IN)
gpio.setup(Left_LED_IN2, gpio.OUT)
gpio.setup(Left_LED_IN1, gpio.OUT)
gpio.setup(Right_LED_IN2, gpio.OUT)
gpio.setup(Right_LED_IN1, gpio.OUT)
gpio.setup(LCM_BL, gpio.OUT)
gpio.setup(LCM_DP, gpio.OUT)

if __name__ == '__main__':
  from threading import Thread
  thread1 = Thread(target=thread_gpiostauts)
  thread1.daemon = True
  thread1.start()
  thread2 = Thread(target=thread_display)
  thread2.daemon = True
  thread2.start()
  print 'Starting server, use <Ctrl-C> to stop'
  while True:
    print 'Display Backlight        : %s' % ( 'True' if gpio.input(LCM_BL) else 'False' )
    print 'Left  Locker is Existed  : %s' % ( 'False' if gpio.input(Left_Lock_Open) else 'True' )
    print 'Left  Locker is Locked   : %s' % ( 'False' if gpio.input(Left_Lock) else 'True' )
    print 'Left  Bike   is Existed  : %s' % ( 'False' if gpio.input(Left_Bike) else 'True' )
    print 'Left  Led                : %s' % lled
    print 'Right Locker Existed     : %s' % ( 'False' if gpio.input(Right_Lock_Open) else 'True' )
    print 'Right Locker is Locked   : %s' % ( 'False' if gpio.input(Right_Lock) else 'True' )
    print 'Right Bike   is Existed  : %s' % ( 'False' if gpio.input(Right_Bike) else 'True' )
    print 'Right Led                : %s' % rled
    time.sleep(0.1)
    