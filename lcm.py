#!/usr/bin/python
# -*- coding: utf-8 -*-
from gpio_mux import *

import Image , ImageFont , ImageDraw
import textwrap
import spidev

gpio.output ( LCM_DP , 0 )
spi = spidev.SpiDev ( )
spi.open ( 0 , 2 )
spi.max_speed_hz = int ( 6 * 1000 * 1000 )
spi.cshigh = True
spi.writebytes ( [ 0x20 , 0x00 ] )
gpio.output ( LCM_DP , 1 )

pixel_bit = 1
columns = 640
# row_offset = 0
row_offset = 80
# rows = 480
rows = 320
rlength = columns * pixel_bit / 8
section = 2
mode = 0x88
black = 0
white = 1

font = ImageFont.truetype ( "mingliu.ttc" , 28 )
line_width = 80


def wallpaper ( filename ):
    import base64
    fp = open ( filename , "r" )
    data = fp.read ( )
    fp.close ( )
    content = map ( ord , list ( base64.b64decode ( data ) ) )
    total_data = [ ]
    for j in range ( 1 , 480 + 1 ):
        total_data.extend ( [ 0x80 | (j >> 8) , j & 0xff ] )
        total_data.extend ( content[ (j - 1) * 240: j * 240 ] )
    for k in range ( 0 , 240 ):
        section_data = total_data[ (480 * (240 + 2) / section) * k:(480 * (240 + 2) / section) * (k + 1) ]
        section_data.append ( 0x00 )
        spi.writebytes ( section_data )


def display ( text , wrap=False ):
    im = Image.new ( "1" , (columns , rows) , black )
    draw = ImageDraw.Draw ( im )
    if wrap is True:
        lines = textwrap.wrap ( text , width=line_width )
    else:
        lines = text.split ( "\n" )
    y_text = 0
    x_text = 0
    l = 0
    for line in lines:
        width , height = font.getsize ( line )
 
	#print("width:"+str(width)+"   height:"+str(height)); ##

        # draw.text(((columns - width) / 2, y_text), line, font=font, fill=white)
        draw.text ( (x_text , y_text) , unicode ( line ) , font=font , fill=white )
        y_text += height
        l += 1
        if l == 24:
            y_text = 0
            x_text = 320

    content = map ( ord , list ( im.tostring ( ) ) )

    toal_data = [ ]
    for j in range ( 1 , rows + 1 ):
        toal_data.extend ( [ mode | ((row_offset + j) >> 8) , (row_offset + j) & 0xff ] )
        toal_data.extend ( content[ (j - 1) * rlength: j * rlength ] )
    for k in range ( 0 , section ):
        section_data = toal_data[ (rows * (rlength + 2) / section) * k:(rows * (rlength + 2) / section) * (k + 1) ]
        section_data.append ( 0x00 )
        spi.writebytes ( section_data )


if __name__ == '__main__':
    fp = open ( "text.txt" )
    data = unicode ( fp.read ( ) )
    fp.close ( )
    display ( data )
