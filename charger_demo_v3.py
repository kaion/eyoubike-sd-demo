#!/usr/bin/python
# -*- coding: utf-8 -*-
import time
import os
import json
import thread_can

def seven_segment_display(pillar):
  prev_lcm = ''
  this_lcm = ''
  pillar['system'] = {'can0' : {},'can1' : {}}
  this_t = int(time.time())
  prev_t = int(time.time()) + 2
  while True:
    if pillar['right'].get('can', False) != False and pillar['left'].get('can', False) != False:
      if pillar['right']['can'].get('interface', False) != False and pillar['left']['can'].get('interface', False) != False:
        pillar['right']['can']['work'] = True
        pillar['left']['can']['work'] = True
        break
    time.sleep(1)
  while True:
    for direction in ['right','left']:
      interface = pillar[direction]['can']['interface']
      this_interface = pillar['system'][interface]
      this_direction = pillar[direction]['can']
      if this_direction['battery'] == True:
        if this_direction['power'] >= 100:
          this_interface['lcm_battery'] = '100'
        elif this_direction['power'] >= 10:
          this_interface['lcm_battery'] = '_%2d' % this_direction['power']
        else:
          this_interface['lcm_battery'] = '__%1d' % this_direction['power']
          
        if this_direction['battery_charging'] == True:
          this_interface['lcm_charging'] = '/'
        else:
          this_interface['lcm_charging'] = '_'
          
      else:
        this_interface['lcm_battery'] = '---'
        this_interface['lcm_charging'] = '-'

      if this_direction['charger'] == True:
        if this_direction['charger_charging'] == True:
          this_interface['lcm_charger'] = 'O'
        else:
          this_interface['lcm_charger'] = 'o'
      else:
        this_interface['lcm_charger'] = '-'
      
      
    this_lcm = pillar['system']['can0']['lcm_charging'] + pillar['system']['can0']['lcm_battery'] + \
               pillar['system']['can1']['lcm_charging']+pillar['system']['can1']['lcm_battery'] + \
               pillar['system']['can1']['lcm_charger'] + pillar['system']['can0']['lcm_charger']
    this_t = int(time.time())
    if this_lcm != prev_lcm or this_t > prev_t:
      if this_t % 2 == 0:
        os.system('./7-segment-display ' + this_lcm + ' _______.__' )
      else:
        os.system('./7-segment-display ' + this_lcm + ' __________' )
      prev_lcm = this_lcm
      prev_t = this_t
    time.sleep(0.2)

if __name__ == "__main__":
  from threading import Thread
  
  pillar = {'left' : {} ,'right' : {} ,'system' : {}}
  thread1 = Thread(target=thread_can.action,args=[pillar,'left'])
  thread1.daemon = True
  thread1.start()
  thread2 = Thread(target=thread_can.action,args=[pillar,'right'])
  thread2.daemon = True
  thread2.start()
  thread3 = Thread(target=seven_segment_display,args=[pillar])
  thread3.daemon = True
  thread3.start()
  print 'Starting server, use <Ctrl-C> to stop'
  
  while True:
    if thread1.isAlive() == False:
      exit()
    if thread2.isAlive() == False:
      exit()
    if thread3.isAlive() == False:
      exit()
    os.system('clear')
    print json.dumps(pillar,sort_keys=True,indent=2)
    time.sleep(1)

