#!/usr/bin/python
# -*- coding: utf-8 -*-
from gpio_mux import *
import time

#NONE = 0
#GREEN = 1
#BLUE = 2
#RED = 3
#target = ['NONE','GREEN','BLUE','RED']
#direct = ['Left','Right']

def action( direction, purpose ):
  direction = direction.upper()
  purpose = purpose.upper()
  if direction == 'LEFT':
    if purpose == 'OFF':
      gpio.output(Left_LED_IN1, 0)
      gpio.output(Left_LED_IN2, 0)
    elif purpose == 'GREEN':
      gpio.output(Left_LED_IN1, 0)
      gpio.output(Left_LED_IN2, 1)
    elif purpose == 'RED':
      gpio.output(Left_LED_IN1, 1)
      gpio.output(Left_LED_IN2, 1)
    elif purpose == 'BLUE':
      gpio.output(Left_LED_IN2, 0)
      gpio.output(Left_LED_IN1, 1)
    else:
      return False
  elif direction == 'RIGHT':
    if purpose == 'OFF':
      gpio.output(Right_LED_IN1, 0)
      gpio.output(Right_LED_IN2, 0)
    elif purpose == 'GREEN':
      gpio.output(Right_LED_IN1, 0)
      gpio.output(Right_LED_IN2, 1)
    elif purpose == 'RED':
      gpio.output(Right_LED_IN1, 1)
      gpio.output(Right_LED_IN2, 1)
    elif purpose == 'BLUE':
      gpio.output(Right_LED_IN2, 0)
      gpio.output(Right_LED_IN1, 1)
    else:
      return False
  elif direction == 'BACKLIGHT':
    if purpose == 'OFF':
      gpio.output(LCM_BL, 0)
    elif purpose == 'ON':
      gpio.output(LCM_BL, 1)
  elif direction == 'BUZZER':
    if purpose == 'OFF':
      gpio.output(BUZZER_CTL_EN, 0)
    elif purpose == 'ON':
      gpio.output(BUZZER_CTL_EN, 1)
  elif direction == 'DISPLAY':
    if purpose == 'OFF':
      gpio.output(LCM_DP, 0)
    elif purpose == 'ON':
      gpio.output(LCM_DP, 1)
  else:
    return False
  return True
  
if __name__ == '__main__':
  dir = 'Right'
  #print action( dir, 'GREEN' )
  #time.sleep(1)
  print action( dir, 'BLUE' )
  time.sleep(0.5)
  print action( dir, 'NONE' )
  time.sleep(0.5)
  print action( dir, 'BLUE' )
  time.sleep(0.5)
  print action( dir, 'NONE' )
  time.sleep(0.5)
  print action( dir, 'BLUE' )
  time.sleep(0.5)
  print action( dir, 'NONE' )
  time.sleep(0.5)
  print action( dir, 'BLUE' )
  time.sleep(0.5)
  print action( dir, 'NONE' )
  time.sleep(0.5)
  print action( dir, 'BLUE' )
  time.sleep(0.5)
  print action( dir, 'NONE' )
  time.sleep(0.5)
  
  