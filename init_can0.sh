#!/bin/sh
exit
PATH=/usr/local/bin:/usr/bin:/bin:/usr/local/sbin:/usr/sbin:/sbin
ifconfig can0 down
ip link set can0 type can bitrate 500000 triple-sampling on
ifconfig can0 up
