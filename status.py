#!/usr/bin/python
# -*- coding: utf-8 -*-
from gpio_mux import *

def update():
  '''
  return { "lcm_bl" : gpio.read(LCM_BL), "right_led_in1" : gpio.read(Right_LED_IN1),
           "right_led_in2" : gpio.read(Right_LED_IN2), "left_led_in1" : gpio.read(Left_LED_IN1),
           "left_led_in2" : gpio.read(Left_LED_IN2), "left_lock_open" : gpio.read(Left_Lock_Open),
           "left_lock" : gpio.read(Left_Lock), "left_bike" : gpio.read(Left_Bike),
           "right_lock_open" : gpio.read(Right_Lock_Open), "right_lock" : gpio.read(Right_Lock),
           "right_bike" : gpio.read(Right_Bike), "left_motor_p" : gpio.read(Left_Motor_P),
           "left_motor_n" : gpio.read(Left_Motor_N), "right_motor_p" : gpio.read(Right_Motor_P),
           "right_motor_n" : gpio.read(Right_Motor_N) }
  '''
  return { 
           "system" : {
             "lcm_bl" : gpio.read(LCM_BL), 
             "buzzer" : gpio.read(BUZZER_CTL_EN),
           },
           "left" : {
             "led_in1" : gpio.read(Left_LED_IN1),
             "led_in2" : gpio.read(Left_LED_IN2), 
             "lock_open" : gpio.read(Left_Lock_Open),
             "lock" : gpio.read(Left_Lock), 
             "bike" : gpio.read(Left_Bike),
             "motor_p" : gpio.read(Left_Motor_P),
             "motor_n" : gpio.read(Left_Motor_N), 
           },
           "right" : {
             "led_in1" : gpio.read(Right_LED_IN1),
             "led_in2" : gpio.read(Right_LED_IN2), 
             "lock_open" : gpio.read(Right_Lock_Open), 
             "lock" : gpio.read(Right_Lock),
             "bike" : gpio.read(Right_Bike), 
             "motor_p" : gpio.read(Right_Motor_P),
             "motor_n" : gpio.read(Right_Motor_N),
           }
         }
if __name__ == '__main__':
  import json
  print json.dumps(update(), sort_keys=True, indent=2)
