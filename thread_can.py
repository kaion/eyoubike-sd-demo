#!/usr/bin/python
# -*- coding: utf-8 -*-

def action(pillar,direction):
  import os
  import can
  import time
  accuracy = 10
  can_interface = 'can0'
  if direction == 'right':
    can_interface='can1'

  os.system('sh init_' + can_interface + '.sh')
  pillar[direction]['can'] = {'charger' : False, 'charger_charging' : False, 'battery' : False, 'power' : 0, 'sleep_t' : 0, 'work_t' : 0, 'work' : False, 'charger_go' : False , 'battery_charging' : False, 'interface' : can_interface, 'prev_22c_t' : 0 }
  this_can = pillar[direction]['can']
  bus = can.interface.Bus(can_interface, bustype='socketcan_ctypes')
  while True:
    t = time.time() 
    now_t = int( t )
    now_t_accuracy = int( t * accuracy )
    
    # 送 CAN 充電訊息
    if this_can['charger'] == True and this_can['battery'] == True and now_t_accuracy > this_can['work_t']:
      if this_can['power'] < 100 and this_can['charger_charging'] is False and this_can['work'] is True:
        msg = can.Message(arbitration_id=0x761, data=[01], extended_id=False)
        try:
          print ("Run charging succ:"+str(msg)) ;##
          bus.send(msg)
          if this_can['charger_go'] is False:
            this_can['charger_go'] = True
        except can.CanError:
          print ("Run charging err:"+str(msg)) ;##
          os.system('sh init_' + can_interface + '.sh')
        
      elif this_can['charger_charging'] is True and ( this_can['work'] is False or this_can['power'] >= 100 ): 
        msg = can.Message(arbitration_id=0x761, data=[00], extended_id=False)
        try:
          print ("Stop charging succ:"+str(msg)) ;##
          bus.send(msg)
          if this_can['charger_go'] is True:
            this_can['charger_go'] = False
        except can.CanError:
          print ("Stop charging err:"+str(msg)) ;##
          os.system('sh init_' + can_interface + '.sh')
          
      this_can['work_t'] = now_t_accuracy + 5 #500ms

    # 收 CAN 訊息
    message = bus.recv(0.001) #(0.1)
    ##print ( "can read:" + str (message)) ;##
    if message is not None:
      this_can['sleep_t'] = 0
      if message.arbitration_id == 0x781 or message.arbitration_id == 0x581:
        this_can['battery'] = True
        this_can['battery_t'] = now_t
        this_can['power'] = message.data[0]
        
      elif message.arbitration_id == 0x7c1:
        this_can['charger'] = True
        this_can['charger_t'] = now_t
        
        if message.data[0] == 0x02 or message.data[0] == 0x04:
          if this_can['charger_charging'] is False:
            this_can['charger_charging'] = True
          
        else:
          if this_can['charger_charging'] is True:
            this_can['charger_charging'] = False

      elif message.arbitration_id == 0x783 and this_can['battery'] == True:
        if ( message.data[4] & 0b00100000 ) > 0:
          this_can['battery_charging'] = True
        else:
          this_can['battery_charging'] = False

    else:
      if this_can['sleep_t'] == 0:
        this_can['sleep_t'] = now_t
      if now_t > this_can['sleep_t'] + 30:
         os.system('sh init_' + can_interface + '.sh')
         this_can['sleep_t'] = 0
    
    #電池離線判斷
    if this_can.get('battery_t', False) != False:
      if this_can['battery'] == True and now_t > this_can['battery_t'] + 1: #多給 n 秒誤差
        print "now_t: %d, battery_t: %d." % ( now_t, this_can['battery_t'] )
        this_can['battery'] = False
        this_can['battery_charging'] = False
        this_can['power'] = 0
        print '%s> this_can["battery"] => False' % can_interface

    #充電器離線判斷
    if this_can.get('charger_t', False) != False:
      if this_can['charger'] == True and now_t > this_can['charger_t'] + 1: #多給 n 秒誤差
        this_can['charger'] = False
        this_can['charger_charging'] = False
        print '%s> this_can["charger"] => False' % can_interface
    
    #充電器離線，詢問電池電量
    if this_can['charger'] == False and now_t_accuracy > this_can['prev_22c_t']:
      msg = can.Message(arbitration_id=0x22C, extended_id=False, is_remote_frame=True)
      try:
        bus.send(msg)
        this_can['prev_22c_t'] = now_t_accuracy + 10 #1000ms
      except can.CanError:
        os.system('sh init_' + can_interface + '.sh')

      