#!/usr/bin/python
# -*- coding: utf-8 -*-
import Image, ImageFont, ImageDraw
import random, os
import spidev
import time
import gpio

DISPLAY_CTRL = True

DISP = 0 

SPI_DEV = 0
SPI_CS = 2

GPIO_DISPLAY = DISP

gpio.setup(GPIO_DISPLAY, gpio.OUT)
gpio.output(GPIO_DISPLAY, 1)

spi = spidev.SpiDev()
spi.open( SPI_DEV, SPI_CS )
spi.mode = 0b00
spi.max_speed_hz = int( 2 * 1000 * 1000 )
spi.cshigh = True

spi.writebytes([0x20,0x00])

pixel_bit = 3
columns = 640
rows = 480
rlength = columns * pixel_bit / 8

section = 2	 #2 - 480, 1 have bug, total_data > 65536?
mode = 0b10000000

def send( total_data ):
  for k in range(0, section):
    section_data = total_data[ ( rows * ( rlength + 2 ) / section ) * k:( rows * ( rlength + 2 ) / section ) * ( k + 1 ) ]
    section_data.append( 0x00 )
    spi.writebytes( section_data )

def display_ctrl( state ):
  if DISPLAY_CTRL == True:
    if state == True:
      gpio.output(GPIO_DISPLAY, 1)
    elif state == False:
      gpio.output(GPIO_DISPLAY, 0)
  
def i2d( im ):
  tmpb = map(ord, im.convert('RGB').tostring())
  content = []
  for j in range( 0, rows ):
    for i in range( 0, columns / 8 ):
      tmp1 = []
      tmp1.extend( tmpb[ j * columns * pixel_bit + i * 8* pixel_bit : j * columns * pixel_bit + ( i + 1 )* 8 * pixel_bit])
      for h in range( 0, 3 ):
        content.append( tmp1[8*h+0]>>7<<7 | tmp1[8*h+1]>>7<<6 | tmp1[8*h+2]>>7<<5 | tmp1[8*h+3]>>7<<4 | tmp1[8*h+4]>>7<<3 | tmp1[8*h+5]>>7<<2 | tmp1[8*h+6]>>7<<1 | tmp1[8*h+7]>>7<<0 )
  total_data = []
  for j in range( 1, rows + 1 ):
    total_data.extend( [ 0x80 | ( j >> 8 ), j & 0xff ] )
    total_data.extend( content[ ( j - 1 ) * rlength : j * rlength ] )
  return total_data

def thread_display():
  import base64
  addr = '/home/root/base64'
  while True:
  #if 1:
   #for filename in ('d01.txt','d03.txt'):
    #filename = 'd01.txt'
    filename = random.choice(os.listdir(addr))
    fp = open( addr + "/" + filename, "r" )
    data = fp.read()
    fp.close()
    content = map( ord, list( base64.b64decode(data) ))
    total_data = []
    for j in range( 1, rows + 1 ):
      total_data.extend( [ mode | ( j >> 8 ), j & 0xff ] )
      total_data.extend( content[ ( j - 1 ) * rlength : j * rlength ] )
    display_ctrl( False )
    send( total_data )
    display_ctrl( True )
    #import sys, Image
    #PALETTE = [ 255, 0, 0, 0, 255, 0, 0, 0, 255, 255, 255, 0, 255, 0, 255, 0, 255, 255, 255, 255, 255, ] + [0, ] * 0xf8 * 3
    #pimage = Image.new("P", (1, 1), 0)
    #pimage.putpalette(PALETTE)
    #while True:
    #  im = Image.open("/home/root/photos/" + random.choice(os.listdir("/home/root/photos"))).convert('RGB')
    #  send( i2d( im.quantize(palette=pimage)))
  
if __name__ == '__main__':

  from threading import Thread
  thread2 = Thread(target=thread_display)
  thread2.daemon = True
  thread2.start()
  print 'Starting server, use <Ctrl-C> to stop'
  while True:
    time.sleep(1)