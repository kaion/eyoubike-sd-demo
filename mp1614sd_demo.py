#!/usr/bin/python
# -*- coding: utf-8 -*-
import status
import led
import locker
import reader
import lcm
from getkey import getkey, keys
from threading import Thread
import time
import os
import json
import sys
import thread_can

accuracy = 10

pillar = {'left' : {} ,'right' : {} ,'system' : {}}

def thread_display():
  #os.system('clear')
  led.action( 'BACKLIGHT', 'ON' )
  #led.action( 'DISPLAY', 'OFF' )
  lcm.wallpaper('color-652058_640.txt')
  lcm.wallpaper('color-1086734_640.txt')
  while True:
    direction = 'left'
    string  = '\n 左車柱 (%s)\n' % pillar[direction]['automatic']['status']
    string += ' 鎖體%s\n' % getlockerstatus( direction )
    string += ' 自行車%s\n' % getbikestatus( direction )
    string += ' 充電器%s\n' % getchargerstatus( direction )
    string += '\n\n\n\n\n\n\n\n\n\n eYouBike Project -alpha 0.1.1.0921-\n\n\n\n\n\n\n\n'
    string += '\n'
    direction = 'right'
    string += '|\n'
    string += '| 右車柱 (%s)\n' % pillar[direction]['automatic']['status']
    string += '| 鎖體%s\n' % getlockerstatus( direction )
    string += '| 自行車%s\n' % getbikestatus( direction )
    string += '| 充電器%s\n' % getchargerstatus( direction )
    string += '|\n|\n|\n|\n|\n|\n|            你好！\n|            Здравствуйте！\n|            HELLO！\n|            こんにちは！\n|                         @Microprogram\n|\n'
    #print pillar['can']
    #string += '右車柱\n鎖體%s\n' % getlockerstatus( 'right' )

    lcm.display( unicode(string) )
    led.action( 'DISPLAY', 'ON' )
    time.sleep(0.5)

def getlockerstatus( direction ):
  string = '偵測中'
  if pillar[direction]['gpio']['lock_open'] is 1:
    string = '未安裝'
  elif pillar[direction]['locker']['mission'] == 'standby':
    if ( pillar[direction]['gpio']['lock'] is 0 and pillar[direction]['gpio']['bike'] is 0 ) or pillar[direction]['automatic']['lock'] == 0:
      string = '已%s' % '上鎖'
    elif pillar[direction]['automatic']['lock'] == 1:
      string = '已%s' % '解鎖'

  elif pillar[direction]['locker']['mission'] == 'unlock':
    if pillar[direction]['gpio']['motor_p'] or pillar[direction]['gpio']['motor_n']:
      string = '%s中' % '解鎖'
    else:
      delay_sec = (pillar[direction]['locker']['delay'] - time.time() * accuracy ) / accuracy
      string = '於 %.1f 秒後%s' % ( delay_sec if delay_sec >= 0 else 0 , '解鎖' )

  elif pillar[direction]['locker']['mission'] == 'lock':
    if pillar[direction]['gpio']['motor_p'] or pillar[direction]['gpio']['motor_n']:
      string = '%s中' % '上鎖'
    else:
      delay_sec = (pillar[direction]['locker']['delay'] - time.time() * accuracy ) / accuracy
      string = '於 %.1f 秒後%s' % ( delay_sec if delay_sec >= 0 else 0 , '上鎖' )

  return string

def getbikestatus( direction ):
  string = '偵測中'
  if pillar[direction]['gpio']['lock_open'] is 1:
    string = '不存在'
  elif pillar[direction]['gpio']['bike'] is 0:
    if pillar[direction].get('can', False) != False:
      if pillar[direction]['can']['battery'] is True:
        if pillar[direction]['can']['battery_charging'] is True:
          string = '存在\n 電池殘量 %d％\n 充電中' % pillar[direction]['can']['power']
        else:
          string = '存在\n 電池殘量 %d％\n 未充電' % pillar[direction]['can']['power']
      else:
        string = '存在\n 電池通訊失敗'
    else:
      string = '存在'

  else:
    string = '不存在'

  return string

def getchargerstatus( direction ):
  string = '偵測中'
  if pillar[direction].get('can', False) != False:
    this_can = pillar[direction]['can']
    if this_can['charger'] is True and this_can['charger_charging'] is True:
      if this_can['work'] != True and this_can['charger_go'] is True:
        string = '工作中\n 發送斷電指令中'
      elif this_can['work'] != True:
        delay_sec = ( this_can['work_t'] - time.time() * accuracy ) / accuracy
        string = '工作中\n 等待 %.1f 秒送斷電指令' % ( delay_sec if delay_sec >= 0 else 0 )
      else:
        string = '工作中'

    elif this_can['charger'] is True and this_can['charger_charging'] is False:
      if this_can['work'] != False and this_can['charger_go'] is True and this_can['battery'] == True:
        string = '待命中\n 發送充電指令中'
      elif this_can['work'] != False and pillar[direction]['can']['power'] < 100 and this_can['battery'] == True:
        delay_sec = ( this_can['work_t'] - time.time() * accuracy ) / accuracy
        string = '待命中\n 等待 %.1f 秒送充電指令' % ( delay_sec if delay_sec >= 0 else 0 )
      else:
        string = '待命中'

    else:
      string = '通訊失敗'

  return string



def thread_getstatus():
  while True:
    #if 1:
    try:
      resp = status.update()
      pillar['left']['gpio'] = resp['left']
      pillar['right']['gpio'] = resp['right']
      pillar['system']['gpio'] = resp['system']
    #else:
    except: # Exception as e:
      print "getstatus failed." #+ str(e)
    time.sleep(0.01)

def thread_getrfid():
  while True:
    for direction in ["left","right"]:
      if pillar[direction].get('reader', False) == False:
        pillar[direction]['reader']= {}
      for target in ["card","tag"]:
        if pillar[direction]['reader'].get(target, False) == False:
          pillar[direction]['reader'][target] = {'respone' : '66-96', 'count' : 0, 'trigger' : False}
        respone_prev = pillar[direction]['reader'][target]["respone"]
        pillar[direction]['reader'][target]["respone"] = reader.action(direction, target)
        if pillar[direction]['reader'][target]["respone"] == respone_prev:
          pillar[direction]['reader'][target]["count"] += 1
        else:
          pillar[direction]['reader'][target]["count"] = 0
          pillar[direction]['reader'][target]["trigger"] = False
    #else:
    #except:
    # print "getrfid failed."
def thread_locker(direction):
  if pillar[direction].get('locker', False) == False:
    pillar[direction]['locker'] = {'mission' : 'standby' ,'completed' : True, 'delay': 0}
  while True:
    for mission in ['lock','unlock']:

      if pillar[direction].get('can', False) != False:
        #if pillar[direction]['locker']['mission'] == mission and pillar[direction]['locker']['completed'] == False and int(time.time() * accuracy ) > pillar[direction]['locker']['delay']:
        if pillar[direction]['locker']['mission'] == mission and pillar[direction]['locker']['completed'] == False and pillar[direction]['can']['charger_charging'] is False and int(time.time() * accuracy ) > pillar[direction]['locker']['delay']:
#          print '%-6slock %-6s %.3f' % (direction, mission, time.time())
          if locker.action( direction, mission ) is True:
            pillar[direction]['locker'] = { 'mission' : 'standby', 'completed' : True, 'delay' : 0 }
          else:
            pillar[direction]['locker'] = { 'mission' : 'standby', 'completed' : True, 'delay' : 0 }
          #pillar['gpio'] = status.update()
            #硬體沒轍...
    time.sleep(0.2)

def thread_led(direction):
  if pillar[direction].get('led', False) == False:
    pillar[direction]['led'] = {'color' : 'off', 'mission' : 'light' ,'timing' : 0 }
  while True:
    if pillar[direction]['led']['mission'] == 'light': #for mission in ['light','flash']:
      led.action( direction, pillar[direction]['led']['color'] )
    elif pillar[direction]['led']['mission'] == 'flash':
      timing = pillar[direction]['led']['timing']
      if timing != 0:
        if ( time.time() * 1000 ) % ( timing * 2 ) > timing:
          led.action( direction, pillar[direction]['led']['color'] )
        else:
          led.action( direction, 'off' )
      else:
        led.action( direction, pillar[direction]['led']['color'] )
    time.sleep(0.05)

def setBuzzerEnable( beep_ms ):
  this_buzzer = pillar['system']['buzzer']
  if this_buzzer['beep'] is False:
    this_buzzer['beep'] = True
    this_buzzer['timing'] = beep_ms
    #print beep_ms
    return True
  return False

def thread_buzzer():
  if pillar['system'].get('buzzer', False) == False:
    pillar['system']['buzzer'] = {'beep' : False, 'time_t': 0, 'timing' : 0.0 }
  while True:
    this_buzzer = pillar['system']['buzzer']
    if this_buzzer['beep'] is True:
      led.action( 'buzzer', 'on' )
      now_t = time.time()
      #print '2- %.3f' % now_t
      this_buzzer['time_t'] = ( now_t * 1000 + this_buzzer['timing'] ) / 1000
      #print '3- %.3f' % this_buzzer['time_t']
      this_buzzer['timing'] = 0
      this_buzzer['beep'] = False
    now_t = time.time()
    #print '1- %.3f' % now_t
    if now_t > this_buzzer['time_t']:
      led.action( 'buzzer', 'off' )
      this_buzzer['time_t'] = 0
    time.sleep(0.05)

def thread_automatic(direction):

  if pillar[direction].get( 'automatic', False) == False:
    pillar[direction]['automatic'] = { 'lock' : 0, 'status' : '啟動中' }
  pillar[direction]['locker'] = { 'mission' : 'lock', 'completed' : False, 'delay' : 0 }
  while True:
    try:
      if pillar[direction].get('gpio', False) != False and pillar[direction]['locker']['completed'] == True:
        #print '.'
        if pillar[direction]['gpio']['lock_open'] is 1:
          pillar[direction]['led'] = {'color' : 'red', 'mission' : 'light' ,'timing' : 0}
          pillar[direction]['automatic']['status'] = '鎖體偵測異常'

        elif pillar[direction]['automatic']['lock'] is 1 and pillar[direction]['gpio']['bike'] is 1: # 鎖體解鎖, 車不存在
          pillar[direction]['automatic']['status'] = '等待還車'
          pillar[direction]['led'] = {'color' : 'blue', 'mission' : 'light' ,'timing' : 0}
          pillar[direction]['locker'] = { 'mission' : 'lock', 'completed' : False, 'delay' : int(time.time() * accuracy) + 2 * accuracy }
          pillar[direction]['automatic']['lock'] = 0 # fix locker bug.

        elif pillar[direction]['automatic']['lock'] is 0 and pillar[direction]['gpio']['bike'] is 1: # 鎖體上鎖, 車不存在
          pillar[direction]['automatic']['status'] = '待還車'
          pillar[direction]['led'] = {'color' : 'blue', 'mission' : 'light' ,'timing' : 0}

        elif pillar[direction]['gpio']['lock'] is 1 and pillar[direction]['gpio']['bike'] is 0: # 鎖體解鎖, 車存在
          pillar[direction]['automatic']['status'] = '請取車'
          pillar[direction]['led'] = {'color' : 'blue', 'mission' : 'flash' ,'timing' : 1000}

        elif pillar[direction]['gpio']['lock'] is 0 and pillar[direction]['gpio']['bike'] is 0: # 鎖體上鎖, 車存在
          if pillar[direction]['locker'].get('completed', False) != False:
            pillar[direction]['automatic']['status'] = '等待借車'
            pillar[direction]['led'] = {'color' : 'green', 'mission' : 'light' ,'timing' : 0}
            if pillar[direction]['automatic']['lock'] == 0:
              changeChargingStatus( direction, True, 5 )

          if pillar[direction]['reader']['card']["respone"] != '66-96' and pillar[direction]['reader']['card']["trigger"] is False:
            setBuzzerEnable( 500 )
            if pillar[direction]['can']['charger_charging'] is False:
              pillar[direction]['automatic']['status'] = '放車中'
              pillar[direction]['locker'] = { 'mission' : 'unlock', 'completed' : False, 'delay' : 0}
              changeChargingStatus( direction, False, -1 )

            else:
              pillar[direction]['automatic']['status'] = '斷電中, 稍後放車'
              pillar[direction]['led'] = {'color' : 'green', 'mission' : 'flash' ,'timing' : 500}
              changeChargingStatus( direction, False, -1 )

              waitChargingDisabled(direction)
              pillar[direction]['automatic']['status'] = '斷電完成, 放車中'
              pillar[direction]['locker'] = { 'mission' : 'unlock', 'completed' : False, 'delay' : int( time.time() * accuracy ) + 2 * accuracy }

            pillar[direction]['reader']['card']["trigger"] = True
            pillar[direction]['automatic']['lock'] = 1 # fix locker bug.
    #else:
    except:
      print "automatic failed."
    time.sleep(0.2)

def changeChargingStatus(direction, status, delaytime):
  if pillar[direction].get('can', False) != False:
    this_can = pillar[direction]['can']
    if this_can['work'] is not status:
#      print 'Charging %-5s A  %.3f' % (status, time.time())
      this_can['work'] = status
      this_can['work_t'] = int( time.time() * accuracy ) + ( delaytime * accuracy )
      this_can['charger_go'] = False
      return True
  return False

def waitChargingDisabled(direction):
  while True:
    if pillar[direction]['can']['charger_charging'] is False:
      break
    time.sleep(0.1)


if __name__ == '__main__':
  thread1 = Thread(target=thread_getstatus)
  thread1.daemon = True
  thread1.start()
  thread2 = Thread(target=thread_buzzer)
  thread2.daemon = True
  thread2.start()
  thread3 = Thread(target=thread_getrfid)
  thread3.daemon = True
  thread3.start()

  thread6 = Thread(target=thread_can.action,args=[pillar,'left'])
  thread6.daemon = True
  thread6.start()
  thread9 = Thread(target=thread_can.action,args=[pillar,'right'])
  thread9.daemon = True
  thread9.start()

  thread5 = Thread(target=thread_led, args=['left'])
  thread5.daemon = True
  thread5.start()
  thread8 = Thread(target=thread_led, args=['right'])
  thread8.daemon = True
  thread8.start()


  thread4 = Thread(target=thread_locker, args=['left'])
  thread4.daemon = True
  thread4.start()
  thread7 = Thread(target=thread_locker, args=['right'])
  thread7.daemon = True
  thread7.start()


  threade = Thread(target=thread_automatic, args=['left'])
  threade.daemon = True
  threade.start()
  threadf = Thread(target=thread_automatic, args=['right'])
  threadf.daemon = True
  threadf.start()

  threadg = Thread(target=thread_display)
  threadg.daemon = True
  threadg.start()

  print 'Starting server, use <Ctrl-C> to stop'

  while True:
    for this_thread in [thread1,thread2,thread3,thread4,thread5,thread6,thread7,thread8,thread9,threade,threadf,threadg]:
      if this_thread.isAlive() == False:
        exit()
    #os.system('clear')
    #print json.dumps(pillar,sort_keys=True,indent=2)
    time.sleep(1)
